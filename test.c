#include <stdio.h>
#include "fbutils.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

static int palette[] = {
	0x000000, 0xffe080, 0xffffff, 0xe0c0a0, 0xff0000
};
#define NR_COLORS (sizeof(palette) / sizeof(palette[0]))

// write ip address from first ethernet adapter with valid address
// which is not "lo" to given buffer
bool getIPAdress(char* output)
{
	struct ifaddrs *ifaddr, *ifa;
	int s;

	if (getifaddrs(&ifaddr) == -1)
	{
		perror("getifaddrs");
		exit(EXIT_FAILURE);
	}

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
	{
		if (ifa->ifa_addr != NULL &&
			ifa->ifa_addr->sa_family == AF_INET)
		{
			s = getnameinfo(ifa->ifa_addr,
                           (ifa->ifa_addr->sa_family == AF_INET) ? sizeof(struct sockaddr_in) :
                                                 sizeof(struct sockaddr_in6),
                           output, NI_MAXHOST,
                           NULL, 0, NI_NUMERICHOST);
			if(s==0)
			{
				if(strcmp(ifa->ifa_name,"lo")==0)
				{
					continue;
				}
				return true;
			}
		}
	}
	return false;
}

int main()
{
	unsigned int i;            // count variable for color palette
	char ipaddr[NI_MAXHOST];   // buffer for IP address
	
    if(open_framebuffer())
	{
		close_framebuffer();
		exit(1);
	}
	
    // set color pallette
    for (i = 0; i < NR_COLORS; i++)
		setcolor(i, palette[i]);

    // get IP address if possible
	if(getIPAdress(ipaddr))
	{
	   put_string_center(xres / 2, yres / 4 + 40, ipaddr, 2);	
	}
	
	close_framebuffer();
	return(0);
}
